/**
 * Created by levine on 15. 2. 6..
 */
var async = require('async');
var _ = require('lodash');
var util = require('util');
var validator = require('validator');
var validPassword = require('./password').getInstance({});
var validEmpty = require('./empty').getInstance({});


exports.getInstance = function() {
    return new OrgUser();
};

function OrgUser(){
    //회원가입
    this.reg = insertUser;

    //로그인
    this.login = selectUserToLogin;
}

/**
 * 회원 가입.
 * @param params
 * @param cb
 */
function insertUser(params) {
    var result = {
        msg: null,
        valid: false
    };

    if (!validEmpty.valid(params.name)) {
        result.msg = '회사 이름을 입력해 주세요.';
        result.valid = false;
        return result;
    }

    if (20 < params.name.toString().trim().length) {
        result.msg = '회사 이름은 20자를 넘을 수 없습니다.';
        result.valid = false;
        return result;
    }

    if (!validEmpty.valid(params.contact)) {
        result.msg = '휴대폰 번호를 입력해 주세요.';
        result.valid = false;
        return result;
    }

    if (!validator.isNumeric(params.contact.toString().trim())) {
        result.msg = '휴대폰 번호는 숫자만 입력해 주세요.';
        result.valid = false;
        return result;
    }

    if (10 > params.contact.toString().trim().length || 11 < params.contact.toString().trim().length) {
        result.msg = '정상적인 번호를 입력해 주세요.';
        result.valid = false;
        return result;
    }

    if (!validEmpty.valid(params.owner)) {
        result.msg = '대표자 이름을 입력해 주세요.';
        result.valid = false;
        return result;
    }

    if (!validEmpty.valid(params.buz_number)) {
        result.msg = '사업자 번호를 입력해 주세요.';
        result.valid = false;
        return result;
    }

    if (!validEmpty.valid(params.password)) {
        result.msg = '비밀번호를 입력해 주세요.';
        result.valid = false;
        return result;
    }

    result.msg = '';
    result.valid = true;

    return result;
}

/**
 * 회원 로그인
 * @param params
 * @param cb
 */
function selectUserToLogin(params){
    var result = {
        msg: null,
        valid: false
    };


    if (!validEmpty.valid(params.name)) {
        result.msg = '이름을 입력해 주세요.';
        result.valid = false;
        return result;
    }

    if (20 < params.name.toString().trim().length) {
        result.msg = '이름은 20자를 넘을 수 없습니다.';
        result.valid = false;
        return result;
    }


    if (!validEmpty.valid(params.password)) {
        result.msg = '비밀번호를 입력해 주세요.';
        result.valid = false;
        return result;
    }

    if (20 < params.password.toString().trim().length) {
        result.msg = '비밀번호는 20자를 넘을 수 없습니다.';
        result.valid = false;
        return result;
    }

    result.msg = '';
    result.valid = true;

    return result;
}
