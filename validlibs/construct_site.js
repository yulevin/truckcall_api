/**
 * Created by levine on 15. 2. 6..
 */
var async = require('async');
var _ = require('lodash');
var util = require('util');
var validator = require('validator');
var validPassword = require('./password').getInstance({});
var validEmpty = require('./empty').getInstance({});


exports.getInstance = function() {
    return new ConstructSite();
};

function ConstructSite(){
    //현장등록
    this.reg = insertConSite;

    //로그인
    this.login = selectUserToLogin;

    //정보조회
    this.find = selectConSite;
}

/**
 * 회원 가입.
 * @param params
 * @param cb
 */
function insertConSite(params) {
    var result = {
        msg: null,
        valid: false
    };


    if (!validEmpty.valid(params.name)) {
        result.msg = '현장 이름을 입력해 주세요.';
        result.valid = false;
        return result;
    }

    if (20 < params.name.toString().trim().length) {
        result.msg = '회사 이름은 20자를 넘을 수 없습니다.';
        result.valid = false;
        return result;
    }

    if (!validEmpty.valid(params.addr)) {
        result.msg = '현장 주소를 입력해 주세요.';
        result.valid = false;
        return result;
    }

//우선 차단 lat : 37.4544266, lon: 127.1309902 validation 오류
    // if (!validator.isLatLong(params.lat) || !validator.isLatLong(params.lon)) {
    //     result.msg = '현장 좌표를 입력해 주세요.';
    //     result.valid = false;
    //     return result;
    // }

    if (!validEmpty.valid(params.to_addr)) {
        result.msg = '목적 현장 주소를 입력해 주세요.';
        result.valid = false;
        return result;
    }
//우선 차단 to_lat : 37.4544266, to_lon: 127.1309902 validation 오류
    // if (!validator.isLatLong(params.to_lat) || !validator.isLatLong(params.to_lon)) {
    //     result.msg = '목적 현장 좌표를 입력해 주세요.';
    //     result.valid = false;
    //     return result;
    // }

    //if (!validEmpty.valid(params.contact)) {
    //    result.msg = '담당자 연락처를 입력해 주세요.';
    //    result.valid = false;
    //    return result;
    //}

    //if (!validator.isNumeric(params.contact.toString().trim())) {
    //    result.msg = '담당자 연락처는 숫자만 입력해 주세요.';
    //    result.valid = false;
    //    return result;
    //}
    //
    //if (10 > params.contact.toString().trim().length || 11 < params.contact.toString().trim().length) {
    //    result.msg = '정상적인 번호를 입력해 주세요.';
    //    result.valid = false;
    //    return result;
    //}

    if (!validator.isInt(params.site_type, {min : 0, max : 9}) ||
        !validator.isInt(params.soil_type, {min : 0, max : 9}) ||
        !validator.isInt(params.truck_type, {min : 0, max : 9})) {

        result.msg = '정확한 현장정보를 입력하세요.';
        result.valid = false;
        return result;
    }

    if (!validator.isInt(params.fee)) {
        result.msg = '운반 요금을 입력하세요.';
        result.valid = false;
        return result;
    }

    if (!validator.isInt(params.quantity)) {
        result.msg = '토석 수량을 입력하세요.';
        result.valid = false;
        return result;
    }

    result.msg = '';
    result.valid = true;

    return result;
}

/**
 * 회원 로그인
 * @param params
 * @param cb
 */
function selectUserToLogin(params){
    var result = {
        msg: null,
        valid: false
    };


    if (!validEmpty.valid(params.name)) {
        result.msg = '이름을 입력해 주세요.';
        result.valid = false;
        return result;
    }

    if (20 < params.name.toString().trim().length) {
        result.msg = '이름은 20자를 넘을 수 없습니다.';
        result.valid = false;
        return result;
    }


    if (!validEmpty.valid(params.password)) {
        result.msg = '비밀번호를 입력해 주세요.';
        result.valid = false;
        return result;
    }

    if (20 < params.password.toString().trim().length) {
        result.msg = '비밀번호는 20자를 넘을 수 없습니다.';
        result.valid = false;
        return result;
    }

    result.msg = '';
    result.valid = true;

    return result;
}

/**
 * 현장정보 조회
 * @param params
 * @param cb
 */
function selectConSite(params){
    var result = {
        msg: null,
        valid: false
    };


    if (!validEmpty.valid(params.site_idx)) {
        result.msg = '현장 정보가 없습니다.';
        result.valid = false;
        return result;
    }

    result.msg = '';
    result.valid = true;

    return result;
}