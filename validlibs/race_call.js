/**
 * Created by levine on 15. 2. 6..
 */
var async = require('async');
var _ = require('lodash');
var util = require('util');
var validator = require('validator');
var validPassword = require('./password').getInstance({});
var validEmpty = require('./empty').getInstance({});


exports.getInstance = function() {
    return new RaceCall();
};

function RaceCall(){
    //회원가입
    this.reg = insertRace;
    this.mod = updateRace;
}

/**
 * 운행 등록.
 * @param params
 * @param cb
 */
function insertRace(params) {
    var result = {
        msg: null,
        valid: false
    };

    if (!params.site_idx) {
        result.msg = '현장 정보가 없습니다.';
        result.valid = false;
        return result;
    }

    if (!params.company_idx) {
        result.msg = '업체 정보가 없습니다.';
        result.valid = false;
        return result;
    }

    if (!params.driver_idx) {
        result.msg = '트럭기사 정보가 없습니다.';
        result.valid = false;
        return result;
    }

    result.msg = '';
    result.valid = true;

    return result;
}


/**
 * 운행 업데이트.
 * @param params
 * @param cb
 */
function updateRace(params) {
    var result = {
        msg: null,
        valid: false
    };

    if (!params.id) {
        result.msg = '운행 정보가 없습니다.';
        result.valid = false;
        return result;
    }

    result.msg = '';
    result.valid = true;

    return result;
}
