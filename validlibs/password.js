/**
 * Created by levine on 15. 2. 9..
 */
var async = require('async');
var _ = require('lodash');
var util = require('util');
var validator = require('validator');


exports.getInstance = function() {
    return new Password();
};

function Password(){
    this.valid = password;
}

function password(param){
    //var chk_num = param.search(/[0-9]/g);
    //var chk_eng = param.search(/[a-z]/ig);

    return (validator.isAlphanumeric(param.toString()) && param.toString().trim().length >= 8 && param.toString().trim().length <= 16 )
}