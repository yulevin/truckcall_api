/**
 * Created by levine on 15. 2. 9..
 */
var async = require('async');
var _ = require('lodash');
var util = require('util');


exports.getInstance = function() {
    return new Empty();
};

function Empty(){
    this.valid = empty;
}

/**
 * 값이 비어있는지 체크한다. 비어있으면 true. 비어있지 않으면 false.
 * @param param
 * @returns {*|boolean}
 */
function empty(param){
    return (_.isUndefined(param) || _.isNull(param) || 1 > param.toString().trim().length) ? false : true;
}