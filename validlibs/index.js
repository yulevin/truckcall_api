/**
 * Created by levine on 15. 2. 6..
 */
var truckUser = require('./truck_user').getInstance({});
var orgUser = require('./org_user').getInstance({});
var conSite = require('./construct_site').getInstance({});
var race = require('./race_call').getInstance({});

exports.truckUser = truckUser;
exports.orgUser = orgUser;
exports.conSite = conSite;
exports.race = race;