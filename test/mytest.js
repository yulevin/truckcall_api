/*
function setBoolean(val) {
    if(val === null || val === undefined || val === '') {
        return null;
    }
    if(val == true) {
        return true;
    }
    return val == 'true';
}

console.log(setBoolean(''));
console.log(setBoolean('false'));
console.log(setBoolean('true'));
console.log(setBoolean(false));
console.log(setBoolean(true));
console.log(setBoolean());
console.log(setBoolean(null));
*/
/*
function findIdSets(text) {
    var pattern=/#{([\w]+)}/g, matches, result = [];
    while (matches = pattern.exec(text)) {
        result.push(matches[1]);
    }
    return result;
}

console.log(findIdSets('저 분 #{set_b_02}최소 배우신 분'));
console.log(findIdSets('저 분 최소 배우#{set_c_11}신 분'));
console.log(findIdSets('저 분 최소 배우신 분#{___}'));
console.log(findIdSets('저 #{a}분 최소#{b} 배우신 분'));
*/
/*
var async = require('async');
var list = [];
for(var i=0;i<72;++i) {
    list.push(i);
}
var len = parseInt(list.length/100,10) + 1;
var funcArray = [];
for(i=0;i<len;++i) {
    console.log(i);
    funcArray.push(getSendFunc(list.slice(100*i, 100*(i+1))));
}

async.series(funcArray, function(err, result) {
    console.log(result);
});

function getSendFunc(list) {
    return function(next) {
        console.log(list.join(','));
        next(null, list.length);
    };
}
*/
/*
var str = ['1', '2', 'ㄱ나', 'null', undefined, null];

for(var i=0;i<str.length;++i) {
    console.log(isNaN(parseInt(str[i], 10)));
}
*/
/*
var num = [1, 3, 5, 5.5, 4.2, 1];

for(var i=0;i<num.length;++i) {
    console.log(~~num[i]);
    console.log(num[i] - ~~num[i]);
}
*/

var hushzoneModel = require('../express/model/newHushzone');
var stream = hushzoneModel.Hushzone.find().limit(100).stream();

stream.on('data', function(doc) {
    console.log(doc);
}).on('error', function(err) {
    console.log(err);
}).on('close', function() {
    console.log('end');
});