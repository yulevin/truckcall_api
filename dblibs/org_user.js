var  async = require('async');
var _ = require('lodash');
var db = require('../lib/dbcon').db;
var util = require('util');


exports.getInstance = function() {
    return new User();
};

function User(){
    //회원가입
    this.reg = insertUser;

    //회원정보수정
    this.modInfo = updateUserToInfo;
    this.modInfoByToken = updateUserToInfoByToken;

    //로그인
    this.loginByInfo = selectUserByInfo;
    this.loginByseq = selectUserBySeq;
    this.loginByToken = selectUserByToken;
    this.findByName = selectUserByName;
}

/**
 * 회원 가입.
 * @param params
 * @param cb
 */
function insertUser(params, cb){
    var query = "INSERT INTO company (name, owner, contact, buz_number, password, access_token) "+
        "VALUES(?, ?, ?, ?, ?, ?)";

    db(query, params, cb);
}

/**
 * 회원 정보 수정
 * @param params
 * @param cb
 */
function updateUserToInfo(params, cb){
    console.log(params);
    var query = "UPDATE company set name = ?, owner = ?, contact = ?, buz_number = ?, password = ?, access_token = ? "+
        "WHERE name = ?, password = ?";

    db(query, params, cb);
}


/**
 * 회원 정보 수정
 * @param params
 * @param cb
 */
function updateUserToInfoByToken(params, cb){
    var query = "UPDATE company set name = ?, owner = ?, contact = ?, buz_number = ?, password = ?, access_token = ? "+
        "WHERE access_token = ?";

    db(query, params, cb);
}

/**
 * 회원 로그인
 * @param params
 * @param cb
 */
function selectUserByToken(params, cb){
    var query = "SELECT " +
        "id, createAt, updateAt, name, owner, contact, buz_number, password, access_token " +
        "FROM company WHERE access_token = ?";

    db(query, params, cb);
}

/**
 * 회원 로그인
 * @param params
 * @param cb
 */
function selectUserBySeq(params, cb){
    var query = "SELECT " +
        "id, createAt, updateAt, name, owner, contact, buz_number, password, access_token " +
        "FROM company WHERE id = ?";

    db(query, params, cb);
}

/**
 * 회원 로그인
 * @param params
 * @param cb
 */
function selectUserByInfo(params, cb){
    var query = "SELECT " +
        "id, createAt, updateAt, name, owner, contact, buz_number, password, access_token " +
        "FROM company WHERE name = ? and password = ?";

    db(query, params, cb);
}

/**
 * 회원 검색
 * @param params
 * @param cb
 */
function selectUserByName(params, cb){
    console.log(params);
    var query = "SELECT " +
        "id, createAt, updateAt, name, owner, contact " +
        "FROM company WHERE name like ?";

    db(query, params, cb);
}
