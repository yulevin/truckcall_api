var  async = require('async');
var _ = require('lodash');
var db = require('../lib/dbcon').db;
var util = require('util');


exports.getInstance = function() {
    return new User();
};

function User(){
    //회원가입
    this.reg = insertUser;

    //회원정보수정
    this.modInfo = updateUserToInfo;
    this.modInfoByToken = updateUserToInfoByToken;
    this.modInfoToPushTokenBySeq = updateUserToPushTokenBySeq;

    //로그인
    this.loginByInfo = selectUserByInfo;
    this.loginByseq = selectUserBySeq;
    this.loginByToken = selectUserByToken;
}

/**
 * 회원 가입.
 * @param params
 * @param cb
 */
function insertUser(params, cb){
    var query = "INSERT INTO driver (name, contact, organizer, organizer_idx, org_type, truck_number, truck_type, push_token, platform, device, access_token) "+
        "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    db(query, params, cb);
}

/**
 * 회원 정보 수정
 * @param params
 * @param cb
 */
function updateUserToInfo(params, cb){
    var query = "UPDATE driver set name = ?, contact = ?, organizer = ?, org_type = ?, truck_number = ?, truck_type = ?, push_token = ?, platform = ?, device = ?, access_token = ? "+
        "WHERE name = ?, contact = ?";

    db(query, params, cb);
}


/**
 * 회원 정보 수정
 * @param params
 * @param cb
 */
function updateUserToInfoByToken(params, cb){
    var query = "UPDATE driver set name = ?, contact = ?, organizer = ?, org_type = ?, truck_number = ?, truck_type = ?, push_token = ?, platform = ?, device = ? "+
        "WHERE access_token = ?";

    db(query, params, cb);
}

/**
 * 회원 정보 수정
 * @param params
 * @param cb
 */
function updateUserToPushTokenBySeq(params, cb){
    var query = "UPDATE driver set push_token = ? "+
        "WHERE id = ?";

    db(query, params, cb);
}

/**
 * 회원 로그인
 * @param params
 * @param cb
 */
function selectUserByToken(params, cb){
    var query = "SELECT " +
        "id, createAt, updateAt, name, contact, organizer, organizer_idx, org_type, truck_number, truck_type, push_token, platform, device, access_token " +
        "FROM driver WHERE access_token = ?";

    db(query, params, cb);
}

/**
 * 회원 로그인
 * @param params
 * @param cb
 */
function selectUserBySeq(params, cb){
    var query = "SELECT " +
        "id, createAt, updateAt, name, contact, organizer, org_type, truck_number, truck_type, push_token, platform, device, access_token " +
        "FROM driver WHERE id = ?";

    db(query, params, cb);
}

/**
 * 회원 로그인
 * @param params
 * @param cb
 */
function selectUserByInfo(params, cb){
    var query = "SELECT " +
        "id, createAt, updateAt, name, contact, organizer, org_type, truck_number, truck_type, push_token, platform, device, access_token " +
        "FROM driver WHERE name = ? and contact = ?";

    db(query, params, cb);
}
