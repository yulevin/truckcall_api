var  async = require('async');
var _ = require('lodash');
var db = require('../lib/dbcon').db;
var nonPrepare = require('../lib/dbcon').nonPrepare;
var util = require('util');


exports.getInstance = function() {
    return new ConstructSite();
};

function ConstructSite(){
    //건설현장 등록
    this.reg = insertSite;

    //건설현장 상태 업데이트
    this.modInfo = updateSiteInfo;
    this.expire = siteExpire;

    //건설현장 조회
    this.sitesBySeq = selectSiteBySeq;
    this.sitesByComp = selectSiteByCompany;
    this.sitesByAddr = selectSiteByAddr;
    this.sitesByName = selectSiteByName;
    this.allSites = selectAllSite;
    this.deleteSite = deleteSite;

    //옵션조회
    this.getTruckType = selectTruckType;
    this.getSiteType = selectSiteType;
    this.getSoilType = selectSoilType;
}

/**
 * 건설현장 등록
 * @param params
 * @param cb
 */
function insertSite(params, cb){
    var query = "INSERT INTO construct_site (company, lat, lon, addr, name, contact, to_lat, to_lon, to_addr, to_name, site_type, soil_type, truck_type, fee, quantity, state, need_quantity) "+
        "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    db(query, params, cb);
}

/**
 * 건설현장 상태 업데이트
 * @param params
 * @param cb
 */
function updateSiteInfo(params, cb){
    var query = "UPDATE construct_site set company = ?, lat, lon = ?, addr = ?, name = ?, contact = ?, to_lat = ?, to_lon = ?, to_addr = ?, to_name = ?, site_type = ?, soil_type = ?, truck_type = ?, fee = ?, quantity = ?, state = ? "+
        "WHERE company = ?";

    db(query, params, cb);
}

/**
 * 건설현장 완료
 * @param params
 * @param cb
 */
function siteExpire(params, cb){
    var query = "UPDATE construct_site set state = 0 "+
        "WHERE company = ?";

    db(query, params, cb);
}

/**
 * 건설현장 조회
 * @param params
 * @param cb
 */
function selectSiteBySeq(params, cb) {
    var query = "SELECT " +
        "consite.id, consite.createAt, consite.updateAt," +
        "consite.lat, consite.lon, consite.addr, consite.name," +
        "consite.contact," +
        "consite.company," +
        "consite.to_lat, consite.to_lon, consite.to_addr, consite.to_name," +
        "consite.site_type, sType.display_name AS display_type," +
        "consite.soil_type, soilType.display_name AS display_soil," +
        "consite.truck_type, tType.display_name AS display_truck," +
        "consite.fee," +
        "consite.quantity," +
        "consite.state," +
        "consite.need_quantity " +
        "FROM construct_site AS consite " +
        "LEFT JOIN site_type AS sType ON consite.site_type = sType.type " +
        "LEFT JOIN soil_type AS soilType ON consite.soil_type = soilType.type " +
        "LEFT JOIN truck_type AS tType ON consite.truck_type = tType.type " +
        "WHERE consite.id = ?";

    db(query, params, cb);
}

/**
 * 건설현장 조회
 * @param params
 * @param cb
 */
function selectSiteByCompany(params, cb) {
    var query =  "SELECT " +
        "consite.id, consite.createAt, consite.updateAt," +
        "consite.lat, consite.lon, consite.addr, consite.name," +
        "consite.contact," +
        "consite.company," +
        "consite.to_lat, consite.to_lon, consite.to_addr, consite.to_name," +
        "consite.site_type, sType.display_name AS display_type," +
        "consite.soil_type, soilType.display_name AS display_soil," +
        "consite.truck_type, tType.display_name AS display_truck," +
        "consite.fee," +
        "consite.quantity," +
        "consite.state," +
        "consite.need_quantity, race.id as race_id, race.state as race_state " +
        "FROM construct_site AS consite " +
        "LEFT JOIN site_type AS sType ON consite.site_type = sType.type " +
        "LEFT JOIN soil_type AS soilType ON consite.soil_type = soilType.type " +
        "LEFT JOIN truck_type AS tType ON consite.truck_type = tType.type " +
        "LEFT JOIN race_call AS race ON consite.id = race.site_idx and race.state = 3 " +
        "WHERE consite.company = ? ";

    db(query, params, cb);
}

/**
 * 건설현장 조회
 * @param params
 * @param cb
 */
function selectSiteByAddr(params, cb) {
    var query = "SELECT " +
        "consite.id, consite.createAt, consite.updateAt," +
        "consite.lat, consite.lon, consite.addr, consite.name," +
        "consite.contact," +
        "consite.company," +
        "consite.to_lat, consite.to_lon, consite.to_addr, consite.to_name," +
        "consite.site_type, sType.display_name AS display_type," +
        "consite.soil_type, soilType.display_name AS display_soil," +
        "consite.truck_type, tType.display_name AS display_truck," +
        "consite.fee," +
        "consite.quantity," +
        "consite.state," +
        "consite.need_quantity " +
        "FROM construct_site AS consite " +
        "LEFT JOIN site_type AS sType ON consite.site_type = sType.type " +
        "LEFT JOIN soil_type AS soilType ON consite.soil_type = soilType.type " +
        "LEFT JOIN truck_type AS tType ON consite.truck_type = tType.type " +
        "WHERE consite.addr like ?";

    db(query, params, cb);
}

/**
 * 건설현장 조회
 * @param params
 * @param cb
 */
function selectSiteByName(params, cb) {
    var query = "SELECT " +
        "consite.id, consite.createAt, consite.updateAt," +
        "consite.lat, consite.lon, consite.addr, consite.name," +
        "consite.contact," +
        "consite.company," +
        "consite.to_lat, consite.to_lon, consite.to_addr, consite.to_name," +
        "consite.site_type, sType.display_name AS display_type," +
        "consite.soil_type, soilType.display_name AS display_soil," +
        "consite.truck_type, tType.display_name AS display_truck," +
        "consite.fee," +
        "consite.quantity," +
        "consite.state," +
        "consite.need_quantity " +
        "FROM construct_site AS consite " +
        "LEFT JOIN site_type AS sType ON consite.site_type = sType.type " +
        "LEFT JOIN soil_type AS soilType ON consite.soil_type = soilType.type " +
        "LEFT JOIN truck_type AS tType ON consite.truck_type = tType.type " +
        "WHERE consite.name like ?";

    db(query, params, cb);
}

function selectSiteType(params, cb){
    var query = "select id, type, display_name from site_type";
    db(query, params, cb);
}
function selectSoilType(params, cb){
    var query = "select id, type, display_name  from soil_type";
    db(query, params, cb);
}
function selectTruckType(params, cb){
    var query = "select id, type, display_name  from truck_type";
    db(query, params, cb);
}

function selectAllSite(cb){
    var query = "SELECT " +
        "consite.id, consite.createAt, consite.updateAt," +
        "consite.lat, consite.lon, consite.addr, consite.name," +
        "consite.contact," +
        "consite.company," +
        "consite.to_lat, consite.to_lon, consite.to_addr, consite.to_name," +
        "consite.site_type, sType.display_name AS display_type," +
        "consite.soil_type, soilType.display_name AS display_soil," +
        "consite.truck_type, tType.display_name AS display_truck," +
        "consite.fee," +
        "consite.quantity," +
        "consite.state," +
        "consite.need_quantity " +
        "FROM construct_site AS consite " +
        "LEFT JOIN site_type AS sType ON consite.site_type = sType.type " +
        "LEFT JOIN soil_type AS soilType ON consite.soil_type = soilType.type " +
        "LEFT JOIN truck_type AS tType ON consite.truck_type = tType.type " +
        "ORDER BY consite.createAt DESC";

    nonPrepare(query, cb);
}

function deleteSite(params, cb){
    var query = " DELETE FROM construct_site WHERE id = ? ";

    db(query, params, cb);
}
