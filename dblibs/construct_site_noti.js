var  async = require('async');
var _ = require('lodash');
var db = require('../lib/dbcon').db;
var nonPrepare = require('../lib/dbcon').nonPrepare;
var util = require('util');


exports.getInstance = function() {
    return new ConstructSite();
};

function ConstructSite(){
    //건설현장 ㅇㅏㄹㄹㅣㅁㅁ 등록
    this.reg = insertSiteNoti;

    //건설현장 상태 업데이트
    this.del = deleteNoti;

    this.findNotiByDriver = selectNotiByDriver;
    this.findNotiBySite = selectNotiBySite;
}

/**
 * 건설현장 등록
 * @param params
 * @param cb
 */
function insertSiteNoti(params, cb){
    var query = "INSERT INTO construct_site_noti (site_idx, driver_idx, type, display_text) "+
        "VALUES(?, ?, ?, ?)";

    db(query, params, cb);
}

/**
 * 건설현장 상태 업데이트
 * @param params
 * @param cb
 */
function deleteNoti(params, cb){
    var query = "DELETE FROM construct_site_noti "+
        "WHERE driver_idx = ?";

    db(query, params, cb);
}

/**
 *
 * @param cb
 */
function selectNotiByDriver(params, cb){
    var where = (params[1] == '-1') ? "WHERE noti.driver_idx = ? and noti.id > ? " : "WHERE noti.driver_idx = ? and noti.id < ? ";
    var query = "SELECT " +
        "noti.id, noti.site_idx, noti.driver_idx, " +
        "noti.display_text, " +
        "consite.addr, consite.name, " +
        "consite.to_addr, consite.to_name, " +
        "consite.fee, " +
        "consite.state " +
        "FROM construct_site_noti AS noti " +
        "LEFT JOIN construct_site AS consite ON consite.id = noti.site_idx " +
        "LEFT JOIN site_type AS sType ON consite.site_type = sType.type " +
        "LEFT JOIN soil_type AS soilType ON consite.soil_type = soilType.type " +
        "LEFT JOIN truck_type AS tType ON consite.truck_type = tType.type " +
        where +
        "ORDER BY noti.createAt DESC " +
        "LIMIT 0, 50";

    db(query, params, cb);
}

/**
 *
 * @param cb
 */
function selectNotiBySite(params, cb){
    var where = (params[1] == '-1') ? "WHERE noti.site_idx = ? and noti.id > ? " : "WHERE noti.site_idx = ? and noti.id < ? ";
    var query = "SELECT " +
        "noti.id, noti.site_idx, noti.driver_idx, " +
        "noti.display_text, " +
        "consite.addr, consite.name, " +
        "consite.to_addr, consite.to_name," +
        "consite.fee," +
        "consite.state " +
        "FROM construct_site_noti AS noti " +
        "LEFT JOIN construct_site AS consite ON consite.id = noti.site_idx " +
        "LEFT JOIN site_type AS sType ON consite.site_type = sType.type " +
        "LEFT JOIN soil_type AS soilType ON consite.soil_type = soilType.type " +
        "LEFT JOIN truck_type AS tType ON consite.truck_type = tType.type " +
        where +
        "ORDER BY noti.createAt DESC " +
        "LIMIT 0, 50";

    db(query, params, cb);
}