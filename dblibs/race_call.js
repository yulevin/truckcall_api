var  async = require('async');
var _ = require('lodash');
var db = require('../lib/dbcon').db;
var util = require('util');


exports.getInstance = function() {
    return new Race();
};

function Race(){
    //건설현장-배차 등록
    this.reg = insertRace;

    //건설현장-배차 상태 업데이트
    this.mod = updateRace;
    this.modState = updateRaceState;
    this.findBySeq = selectRaceBySeq;
    this.findByCompany = selectRaceByCompany;
    this.findBySite = selectRaceBySite;
    this.findBySiteToDriver = selectBySiteToDriver;
    this.findByDriverNSite = selectRaceByDriverNSite;
    this.findByInfo = selectRaceByInfo;
    this.findByDriver = selectRaceByDriver;
    this.findRaceWithDriverNSite = selectRaceWithDriverNSite;
}

/**
 * 배차 등록
 * @param params
 * @param cb
 */
function insertRace(params, cb){
    var query = "INSERT INTO race_call (site_idx, company_idx, driver_idx, state) "+
        "VALUES(?, ?, ?, ?)";

    db(query, params, cb);
}

/**
 * 배차 상태 업데이트
 * @param params
 * @param cb
 */
function updateRace(params, cb){
    var query = "UPDATE race_call set site_idx = ?, company_idx = ?, driver_idx = ?, state = ? "+
        "WHERE id = ?";

    db(query, params, cb);
}

/**
 * 배차 상태 업데이트
 * @param params
 * @param cb
 */
function updateRaceState(params, cb){
    var query = "UPDATE race_call set state = ? "+
        "WHERE id = ?";

    db(query, params, cb);
}


/**
 * 배차 조회
 * @param params
 * @param cb
 */
function selectRaceBySeq(params, cb){
    var query = "SELECT " +
        "race.id, race.createAt, race.updateAt, race.site_idx, race.company_idx, race.driver_idx, race.state, rs.display_name AS display_state " +
        "FROM race_call AS race " +
        "LEFT JOIN race_state AS rs ON race.state = rs.type " +
        "WHERE race.id = ?";

    db(query, params, cb);
}


/**
 * 배차 조회
 * @param params
 * @param cb
 */
function selectRaceByCompany(params, cb){
    var query = "SELECT " +
        "race.id, race.createAt, race.updateAt, race.site_idx, race.company_idx, race.driver_idx, race.state, rs.display_name AS display_state " +
        "FROM race_call AS race " +
        "LEFT JOIN race_state AS rs ON race.state = rs.type " +
        "WHERE race.company_idx = ?";

    db(query, params, cb);
}


/**
 * 배차 조회
 * @param params
 * @param cb
 */
function selectRaceBySite(params, cb){
    var query = "SELECT " +
        "race.id, race.createAt, race.updateAt, race.site_idx, race.company_idx, race.driver_idx, race.state, rs.display_name AS display_state " +
        "FROM race_call AS race " +
        "LEFT JOIN race_state AS rs ON race.state = rs.type " +
        "WHERE race.site_idx = ?";

    db(query, params, cb);
}

/**
 * 배차 조회
 * Web 에서 사용되는 현장정보 상세내 배차신청 정보 조회
 */
function selectBySiteToDriver(params, cb){
    var query = "SELECT  a.id, a.createAt, a.updateAt, a.site_idx, a.company_idx, a.driver_idx, a.state, c.display_name as truck_name " +
        ", b.name, b.contact, b.organizer, b.organizer_idx, b.org_type, b.truck_number, b.truck_type, b.push_token, b.platform, b.device" +
        " FROM race_call a" +
        " left join driver b on a.driver_idx = b.id" +
        " left join truck_type c on b.truck_type = c.id "+
        " WHERE site_idx = ?";

    db(query, params, cb);
}


/**
 * 배차 조회
 * @param params
 * @param cb
 */
function selectRaceByDriver(params, cb){
    var query = "SELECT " +
        "race.id, race.createAt, race.updateAt, race.site_idx, race.company_idx, race.driver_idx, race.state, rs.display_name AS display_state " +
        "FROM race_call AS race " +
        "LEFT JOIN race_state AS rs ON race.state = rs.type " +
        "WHERE race.driver_idx = ?";

    db(query, params, cb);
}

/**
 * 배차 조회
 * @param params
 * @param cb
 */
function selectRaceByDriverNSite(params, cb){
    var query = "SELECT " +
        "race.id, race.createAt, race.updateAt, race.site_idx, race.company_idx, race.driver_idx, race.state, rs.display_name AS display_state " +
        "FROM race_call AS race " +
        "LEFT JOIN race_state AS rs ON race.state = rs.type " +
        "WHERE race.site_idx = ? and race.driver_idx = ?";

    db(query, params, cb);
}


/**
 * 배차 조회
 * @param params
 * @param cb
 */
function selectRaceByInfo(params, cb){
    var query = "SELECT " +
        "race.id, race.createAt, race.updateAt, race.site_idx, race.company_idx, race.driver_idx, race.state, rs.display_name AS display_state " +
        "FROM race_call AS race " +
        "LEFT JOIN race_state AS rs ON race.state = rs.type " +
        "WHERE race.site_idx = ? and race.company_idx = ? and race.driver_idx = ?";

    db(query, params, cb);
}


/**
 * 배차&운전자 조회
 * @param params
 * @param cb
 */
function selectRaceWithDriverNSite(params, cb){
    var query = "SELECT " +
        "race.id, race.createAt, race.updateAt, race.company_idx, race.state, rs.display_name AS display_state, " +
        "dr.id AS driver_id, dr.name AS driver_name, dr.push_token, " +
        "site.id AS site_id, site.name AS site_name, site.to_name  AS site_to_name " +
        "FROM race_call AS race " +
        "LEFT JOIN race_state AS rs ON race.state = rs.type " +
        "LEFT JOIN driver AS dr ON race.driver_idx = dr.id " +
        "LEFT JOIN construct_site AS site ON race.site_idx = site.id " +
        "WHERE race.id = ?";

    db(query, params, cb);
}
