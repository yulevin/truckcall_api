var  async = require('async');
var _ = require('lodash');
var db = require('../lib/dbcon').db;
var util = require('util');


exports.getInstance = function() {
    return new RaceRecord();
};

function RaceRecord(){
    //운행 등록
    this.reg = insertRaceRecord;

    //기사의 운행기록 조회
    this.findByDriver = selectRaceRecordByDriver;
}

/**
 * 운행 등록
 * @param params
 * @param cb
 */
function insertRaceRecord(params, cb){
    var query = "INSERT INTO race_record (createAt, race_idx, site_idx, driver_idx, from_visit, to_visit) "+
        "VALUES(?, ?, ?, ?, ?, ?)";

    db(query, params, cb);
}

/**
 * 운행 조회
 * @param params
 * @param cb
 */
function selectRaceRecordByDriver(params, cb){
    var where = (!params[1]) ? "WHERE record.driver_idx = ? " : "WHERE record.driver_idx = ? and record.createAt < ? ";
    console.log(params);
    console.log(where);
    var query = "SELECT " +
        "date_format(record.createAt, '%Y-%m-%d') AS createAt, count(record.driver_idx) AS cnt, " +
        "date_format(MIN(record.from_visit), '%r') AS startAt, date_format(MAX(record.to_visit), '%r') AS endAt, " +
        "site.name, site.to_name " +
        "FROM race_record AS record " +
        "LEFT JOIN construct_site AS site ON record.site_idx = site.id " +
        where +
        "GROUP BY record.site_idx, date_format(record.createAt, '%Y-%m-%d'), record.race_idx " +
        "ORDER BY record.createAt DESC " +
        "LIMIT 0, 50";

    db(query, params, cb);
}
