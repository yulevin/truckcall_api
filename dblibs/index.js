var truckUser = require('./truck_user').getInstance({});
var orgUser = require('./org_user').getInstance({});
var conSite = require('./construct_site').getInstance({});
var conSiteNoti = require('./construct_site_noti').getInstance({});
var race = require('./race_call').getInstance({});
var raceRecord = require('./race_record').getInstance({});

exports.truckUser = truckUser;
exports.orgUser = orgUser;
exports.conSite = conSite;
exports.conSiteNoti = conSiteNoti;
exports.race = race;
exports.raceRecord = raceRecord;