var _ = require('lodash');
var config = require('../../config').get();
var uuid = require('node-uuid');
var async = require('async');

//MQTT 세션 추가하기
module.exports = [
    {
        method : 'post',
        url : '/api/session/connect',
        func : connectSession,
        middleware : []
    },
    {
        method : 'post',
        url : '/api/session/disconnect',
        func : disconnectSession,
        middleware : []
    }
];

function connectSession(req, res) {
    var user = req.user;
    var sessionId = _.now()+"."+uuid.v4()+"."+(Math.random()*10000).toFixed(0);
    var location = req.param('location')?req.param('location'):'ko';
    var timezone = req.param('timezone')?req.param('timezone'):'ko';
    var session = new sessionModel.Session({
        uid : user._id,
        'sessionId' : sessionId,
        'date' : new Date(),
        'location' : location,
        'timezone' : timezone
    });
    session.save(function(err) {
        if(err) {
            res.json(200, error.getResult(req, 'ERROR_DB', null));
        } else {
            redisModel.addSessionToUser(user._id.toString(), sessionId, function(err) {
                if(err) {
                    res.json(200, error.getResult(req, 'ERROR_DB', null));
                } else {
                    res.json(200, error.getResult(req, null, {'sessionId' : sessionId}));
                }
            });
        }
    });
}

function disconnectSession(req, res) {
    var user = req.user;
    var sessionId = req.param('sessionId');

    if(!sessionId) {
        res.json(200, error.getResult(req, 'INVALID_PARAMS', null));
        return;
    }

    var query = { 'sessionId' : sessionId, uid : user._id, out : null };
    sessionModel.update(query, { 'out' : new Date() }, {safe : true}, function(err) {
        if(err) {
            res.json(200, error.getResult(req, 'ERROR_DB', null));
        } else {
            redisModel.removeSessionFromUser(user._id.toString(), sessionId, function(err) {
                if(err) {
                    res.json(200, error.getResult(req, 'ERROR_DB', null));
                } else {
                    res.json(200, error.getResult(req, null, null));
                }
            });
        }
    });
}