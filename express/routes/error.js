var _ = require('lodash');

exports.getResult = function(req, code, data) {

    var output = {
        status : "",
        error : null,
        data : {}
    };

    if(code) {
        return {
            status : "fail",
            error : code
        };
    } else {
        return {
            status : "success",
            data : data
        };
    }
};
exports.getResult2 = function(req, code, data) {
    var output = {
        message : "",
        error : false,
        result : data
    };
    return output;
};

var ERROR_CODE = {
    'INVALID_HEADER' : "4010",//유효하지 않은 헤더
    'INVALID_PARAMS' : "4011",//유효하지 않은 파라미터
    'INVALID_SESSION' : "4012",//세션 만료
    'INVALID_USER' : "4013",//유효하지 않은 사용자
    'INVALID_PHONE' : "4014",//유효하지 않은 폰번호
    'INVALID_DATE' : "4015",//유효하지 않은 폰번호
    'INVALID_TIME' : "4016",//유효하지 않은 폰번호
    'INVALID_NAME' : "4017",//유효하지 않은 폰번호
    'INVALID_PRICE' : "4018",//유효하지 않은 폰번호
    'ERROR_DB' : "5000",//디비 에러
    'ERROR_INTERNAL' : "5001",//서버 내부 에러
    'SYSTEM_ACCESS_DENIED' : '6666',     //시스템 접근 거부됨
    'NOT_ADMIN' : '8000',     //not admin
    'WRONG_ADMIN_PASSWORD' : '8001',     //admin, but wrong password
    'NEED_ADMIN_INFO' : '8002',     //not admin
    'EMPTY_NAIL_RESERVE' : '4040', //네일 예약 없음
    'EMPTY_TIME_TABLE' : '4041', //테이블 없음 
    'EMPTY_TIME_TABLES' : '4042', //테이블 목록 없음 
    'FAIL_NAIL_RESERVE' : '5010', //네일 예약 실패
    'FAIL_NAIL_UPDATE' : '5011', //네일 수정 실패
    'FAIL_NAIL_DELETE' : '5012', //네일 예약 삭제 실패
    'FAIL_TABLE_REG' : '5020', //시간대 입력 실패
    'FAIL_TABLE_MOD' : '5021', //시간대 수정 실패
    'FAIL_TABLE_DEL' : '5022', //시간대 삭제 실패
    'FAIL_SALE_REG' : '5030', //매출 입력 실패
    'FAIL_SALE_MOD' : '5031', //매출 수정 실패
    'FAIL_SALE_DEL' : '5032' //매출 삭제 실패
};

exports.ERROR_CODE = ERROR_CODE;