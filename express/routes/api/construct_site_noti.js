/**
 * Created by levine on 15. 2. 6..
 */
var _ = require('lodash');
var config = require('../../../config').get();
var error = require('../error');
var async = require('async');
var db = require('../../../dblibs/');
var moment = require('moment');
var util = require('util');
var validate = require('../../../validlibs/');
var encryptLib = require('../../../lib/encrypt');
var random = require('randomstring');
var fcm = require('../../../lib/fcm');

module.exports = [
    {
        method : 'delete',
        url : '/api/sitenoti/delete',
        func : deleteNoti,
        middleware : [
        ]
    },
    {
        method : 'get',
        url : '/api/sitenoti/driver/:driver_id',
        func : getNotiByDriver,
        middleware : [
        ]
    },
    {
        method : 'get',
        url : '/api/sitenoti/site/:site_id',
        func : getNotiBySite,
        middleware : [
        ]
    }
];

/**
 * DEL 노티 삭제
 * @param req
 * @param res
 */
function deleteNoti(req, res){
    console.log(req.body);
    var data = req.body;

    //validate[S]
    var valresult = validate.race.reg(data);
    console.log(valresult);
    if(!valresult.valid)
        return res.json(400, error.getResult(req, valresult.msg, null));

    //validate[E]
    var params = [
        data.site_idx,
        data.company_idx,
        data.driver_idx,
        data.state
    ];

    async.waterfall([
        //이미 가입된 회원인지 체크
        function(next){
            db.race.reg(params, function(err, results, fields) {
                if (err) //db 에러
                    res.json(500, error.getResult(req, 'DB에러가 발생했습니다.', null));
                else if (!results || 1 > results.length || 1 > results.count) //중복 없음
                    res.json(500, error.getResult(req, '등록에 실패했습니다.', null));
                else //이미 가입되어 있으면 로그인 처리
                    next(null, results.insertId);
            });
        }],
        function(err, results){
            console.log("regist complete");
            db.race.findBySeq(results,  function(err, results, fields) {
                if (err) //db 에러
                    res.json(error.getResult(req, 'DB에러가 발생했습니다.', null));
                else if (!results || 1 > results.length || 1 > results.count) //중복 없음
                    res.json(error.getResult(req, '회원가입에 실패했습니다.', null));
                else //이미 가입되어 있으면 로그인 처리
                    res.json(error.getResult(req, null, results[0]));
            });
        }
    );
}

/**
 * GET 노티 조회
 * @param {*} req
 * @param {*} res
 */
function getNotiByDriver(req, res){
    console.log(req.query);
    console.log(req.params);
    var data = req.query;
    var driver_id = req.params.driver_id;

    db.conSiteNoti.findNotiByDriver([driver_id, data.start], function(err, results, fields) {
        if (err) //db 에러
            res.status(500).json(error.getResult(req, 'DB에러가 발생했습니다.', null));
        else if (!results || 1 > results.length || 1 > results.count) //가입되어있지 않으면 에러
            res.status(400).json(error.getResult(req, '알림이 없습니다.', null));
        else //가입되어 있으면 로그인 처리
            res.status(200).json(error.getResult(req, null, results));
    });
}
/**
 * GET 노티 조회
 * @param {*} req
 * @param {*} res
 */
function getNotiBySite(req, res){
    console.log(req.query);
    console.log(req.params);
    var data = req.query;
    var site_id = req.params.site_id;
    db.conSiteNoti.findNotiBySite(site_id, function(err, results, fields) {
        if (err) //db 에러
            res.status(500).json(error.getResult(req, 'DB에러가 발생했습니다.', null));
        else if (!results || 1 > results.length || 1 > results.count) //가입되어있지 않으면 에러
            res.status(400).json(error.getResult(req, '알림이 없습니다.', null));
        else //가입되어 있으면 로그인 처리
            res.status(200).json(error.getResult(req, null, results));
    });
}