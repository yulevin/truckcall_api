/**
 * Created by levine on 15. 2. 6..
 */
var _ = require('lodash');
var config = require('../../../config').get();
var error = require('../error');
var async = require('async');
var fcm = require('../../../lib/fcm');//.test;
var db = require('../../../dblibs/');

module.exports = [
    {
        method : 'get',
        url : '/api/fcm/test',
        func : fcmTest,
        middleware : [
        ]
    },
    {
        method : 'get',
        url : '/api/fcm/topic',
        func : fcmTopicTest,
        middleware : [
        ]
    },
    {
        method : 'get',
        url : '/api/fcm/topic2',
        func : fcmTopicTest2,
        middleware : [
        ]
    }
];

/**
 * 회원가입
 * @param req
 * @param res
 */
function fcmTest(req, res){
    var message = {
        //to: 'news',
        registration_ids: [
            'c53uWZuM4Go:APA91bGxWl2NPKwGbRE65ym4_j9ubKQHTGcdsafowd5eyWpNfnFr8_CiCu40CD5LMzbGcspEPag-psNR37RbfRYxhQCTSzLJzyl1CHw7dOfRmR_gq40tQAlsdW9WRPt5lm-tbZzJSoB6xFp3TYMAtxMqCoWhoSu-MA',
            'fcGmBu7OvtA:APA91bFSyWXBXIaVsZiqgjyS_TV0c5J33Mb7JymqCPpyCFxhQ4ayXnxjWERO2Xgw9TfeO_KltXWxJd_DsRVmYAmS-_KW1pts1uY3OwrEOKwhM9uD2YZvkINYjy0-T1aEf3LwgupETZ-129M8qzpLLF4HoNgiK3xi4A'
        ],
        collapse_key: '368818888192',
        data: {
            your_custom_data_key: 'your_custom_data_value'
        },
        notification: {
            title: 'Title of your push notification',
            body: 'Body of your push notification'
        }
    };

    fcm.test(message, function(err, response){
        if (err) {
            console.log(err);
            console.log("Something has gone wrong!");
            res.status(400).json(err);
        } else {
            console.log(response);
            console.log("Successfully sent with response: ", response);
            res.status(200).json(response);
        }
    });
}

/**
 * 회원가입
 * @param req
 * @param res
 */
function fcmTopicTest(req, res){
    db.conSite.allSites(function(err, results, fields){
        fcm.sendTopic("contruct_site", results[0], null, function(err, response){
            if (err) {
                console.log(err);
                console.log("Something has gone wrong!");
                res.status(400).json(err);
            } else {
                console.log(response);
                console.log("Successfully sent with response: ", response);
                res.status(200).json(response);
            }
        });
    });
}


/**
 * 회원가입
 * @param req
 * @param res
 */
function fcmTopicTest2(req, res){
    db.conSite.allSites(function(err, results, fields){
        var fcmBody = {
            idx : results[0].id,
            filted : false,
            displayName : results[0].name,
            displayToName : results[0].to_name,
            fee : results[0].fee,
            createAt : results[0].createAt
        };
        var notification = {
            title : "새로운 현장 알림",
            body : results[0].name + "에 트럭이 필요합니다."
        };
        fcm.sendTopic("contruct_site", fcmBody, notification, function(err, response){
            if (err) {
                console.log(err);
                console.log("Something has gone wrong!");
                res.status(400).json(err);
            } else {
                console.log(response);
                console.log("Successfully sent with response: ", response);
                res.status(200).json(response);
            }
        });
    });
}
