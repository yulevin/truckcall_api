/**
 * Created by levine on 15. 2. 6..
 */
var _ = require('lodash');
var config = require('../../../config').get();
var error = require('../error');
var async = require('async');
var db = require('../../../dblibs/');
var moment = require('moment');
var util = require('util');
var validate = require('../../../validlibs/');
var encryptLib = require('../../../lib/encrypt');
var random = require('randomstring');

module.exports = [
    {
        method : 'post',
        url : '/api/org_user/reg',
        func : registOrgUser,
        middleware : [
        ]
    },
    {
        method : 'post',
        url : '/api/org_user/login',
        func : loginOrgUser,
        middleware : [
        ]
    },
    {
        method : 'get',
        url : '/api/org_user/find',
        func : getOrganizerByName,
        middleware : [
        ]
    },

];

/**
 * 회원가입
 * @param req
 * @param res
 */
function registOrgUser(req, res){
    console.log(req.body);
    var data = req.body;

    //validate[S]
    var valresult = validate.orgUser.reg(data);
    console.log(valresult);
    if(!valresult.valid)
        return res.status(400).json(error.getResult(req, valresult.msg, null));

    //validate[E]
    var chkParams = [data.name.toString().trim(), data.password.toString().trim()];

    async.waterfall([
        //이미 가입된 회원인지 체크
        function(next){
            db.orgUser.loginByInfo(chkParams, function(err, results, fields) {
                if (err) //db 에러
                    res.status(500).json(error.getResult(req, 'DB에러가 발생했습니다.', null));
                else if (!results || 1 > results.length || 1 > results.count) //중복 없음
                    next(null, null);
                else //이미 가입되어 있으면 로그인 처리
                    saveSession(req, res, results[0]);
            });
        },
        //회원가입
        function(result, next){
            if(null != result && null != next)
                next();
            else {
                var queryParams = [
                    data.name,
                    data.owner,
                    data.contact,
                    data.buz_number,
                    data.password,
                    null
                ];


                db.orgUser.reg(queryParams, function(err, results, fields) {
                    if (err) //db 에러
                        res.status(500).json(error.getResult(req, 'DB에러가 발생했습니다.', null));
                    else if (!results || 1 > results.length || 1 > results.count) //중복 없음
                        res.status(400).json(error.getResult(req, '회원가입에 실패했습니다.', null));
                    else //이미 가입되어 있으면 로그인 처리
                        next(null, results.insertId);
                });
            }
        }],
        function(err, results){
            console.log("regist complete");
            db.orgUser.loginByseq(results,  function(err, results, fields) {
                if (err) //db 에러
                    res.status(500).json(error.getResult(req, 'DB에러가 발생했습니다.', null));
                else if (!results || 1 > results.length || 1 > results.count) //중복 없음
                    res.status(400).json(error.getResult(req, '회원가입에 실패했습니다.', null));
                else //이미 가입되어 있으면 로그인 처리
                    res.status(200).json(error.getResult(req, null, results[0]));
            });
        }
    );
}

/**
 * 로그인
 * @param req
 * @param res
 */
function loginOrgUser(req, res){
    console.log(req.body);
    var data = req.body;

    //validate[S]
    var valresult = validate.orgUser.login(data);
    console.log(valresult);
    if(!valresult.valid)
        return res.status(400).json(error.getResult(req, valresult.msg, null));

    //validate[E]
    var chkParams = [data.name.toString().trim(), data.password.toString().trim()];

    async.waterfall([
            //이미 가입된 회원인지 체크
            function(next){
                db.orgUser.loginByInfo(chkParams, function(err, results, fields) {
                    if (err) //db 에러
                        res.status(500).json(error.getResult(req, 'DB에러가 발생했습니다.', null));
                    else if (!results || 1 > results.length || 1 > results.count) //가입되어있지 않으면 에러
                        res.status(400).json(error.getResult(req, '회원가입을 먼저 해주세요.', null));
                    else //가입되어 있으면 로그인 처리
                        next(null, results[0]);
                });
            }],
        function(err, results){
            console.log("login complete")
            res.status(200).json(error.getResult(req, null, results));
        }
    );
}

function saveSession(req, res, param){
    console.log("exists user");
    res.status(200).json(error.getResult(req, null, param));
}

function getOrganizerByName(req, res){
    console.log(req.query);
    var data = req.query;
    db.orgUser.findByName('%'+data.name.trim()+'%', function(err, results, fields) {
        if (err) //db 에러
            res.status(500).json(error.getResult(req, 'DB에러가 발생했습니다.', null));
        else if (!results || 1 > results.length || 1 > results.count) //가입되어있지 않으면 에러
            res.status(400).json(error.getResult(req, '검색결과가 없습니다.', null));
        else //가입되어 있으면 로그인 처리
            res.status(200).json(error.getResult(req, null, results));
    });
}