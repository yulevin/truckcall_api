/**
 * Created by levine on 15. 2. 6..
 */
var _ = require('lodash');
var config = require('../../../config').get();
var error = require('../error');
var async = require('async');
var db = require('../../../dblibs/');
var moment = require('moment');
var util = require('util');
var validate = require('../../../validlibs/');
var encryptLib = require('../../../lib/encrypt');
var random = require('randomstring');
var fcm = require('../../../lib/fcm');

module.exports = [
    {
        method : 'post',
        url : '/api/race/reg',
        func : registRace,
        middleware : [
        ]
    },
    {
        method : 'post',
        url : '/api/race/update',
        func : updateRace,
        middleware : [
        ]
    },
    {
        method : 'get',
        url : '/api/race/find',
        func : getRaceByCompany,
        middleware : [
        ]
    },
    {
        method : 'get',
        url : '/api/race/site/:siteid',
        func : getRaceBySite,
        middleware : [
        ]
    }
];

/**
 * 배차등록
 * @param req
 * @param res
 */
function registRace(req, res){
    console.log(req.body);
    var data = req.body;

    //validate[S]
    var valresult = validate.race.reg(data);
    console.log(valresult);
    if(!valresult.valid)
        return res.json(400, error.getResult(req, valresult.msg, null));

    //validate[E]
    var params = [
        data.site_idx,
        data.company_idx,
        data.driver_idx,
        data.state
    ];

    async.waterfall([
        //이미 가입된 회원인지 체크
        function(next){
            db.race.reg(params, function(err, results, fields) {
                if (err) //db 에러
                    res.json(500, error.getResult(req, 'DB에러가 발생했습니다.', null));
                else if (!results || 1 > results.length || 1 > results.count) //중복 없음
                    res.json(500, error.getResult(req, '등록에 실패했습니다.', null));
                else //이미 가입되어 있으면 로그인 처리
                    next(null, results.insertId);
            });
        }],
        function(err, results){
            console.log("regist complete");
            db.race.findBySeq(results,  function(err, results, fields) {
                if (err) //db 에러
                    res.json(error.getResult(req, 'DB에러가 발생했습니다.', null));
                else if (!results || 1 > results.length || 1 > results.count) //중복 없음
                    res.json(error.getResult(req, '회원가입에 실패했습니다.', null));
                else //이미 가입되어 있으면 로그인 처리
                    res.json(error.getResult(req, null, results[0]));
            });
        }
    );
}


/**
 * 배차 업데이트
 * @param req
 * @param res
 */
function updateRace(req, res){
    console.log(req.body);
    var data = req.body;

    //validate[S]
    var valresult = validate.race.mod(data);
    console.log(valresult);
    if(!valresult.valid)
        return res.json(400, error.getResult(req, valresult.msg, null));

    //validate[E]
    var params = [data.state, data.id];

    async.waterfall([
            function(next){
                db.race.modState(params, function(err, results, fields) {
                    if (err) //db 에러
                        res.json(500, error.getResult(req, 'DB에러가 발생했습니다.', null));
                    else if (!results || 1 > results.length || 1 > results.count) //중복 없음
                        res.json(500, error.getResult(req, '수정에 실패했습니다.', null));
                    else
                        next(null, data.id);
                });
            },
            function(result, next){
                //상태값에 따라 fcm 발송. 막 등록했을땐 보내지 않는다.
                if(data.state != 1){
                    db.race.findRaceWithDriverNSite([data.id], function(err, results, fields) {
                        if (err) //db 에러
                            console.log(err);
                        else if (!results || 1 > results.length || 1 > results.count) //중복 없음
                            console.log('수정에 실패했습니다.');
                        else {
                            console.log(results[0]);

                            var fcmBody = {
                                idx : results[0].site_id,
                                filted : false,
                                displayName : results[0].site_name,
                                displayToName : results[0].site_to_name
                            };
                            var notification = getFCMMessage(results[0].site_name, data.state);
                            console.log(notification);

                            var noti = [results[0].site_id, results[0].driver_id, 0, notification.body];

                            db.conSiteNoti.reg(noti, function(err, results, fields){
                                if (err)
                                    console.log('err : ' + err);
                                else
                                    console.log('save complete')
                            });

                            fcm.sendSingle(results[0].push_token, fcmBody, notification, function(err, response){
                                if (err) {
                                    console.log(err);
                                    console.log("Something has gone wrong!");
                                } else {
                                    console.log(response);
                                    console.log("Successfully sent with response: ", response);
                                }
                            });
                            next(null, data.id);
                        }
                    });
                } else
                    next(null, data.id);
            }
        ],
        function(err, results){
            console.log("update complete.." + results);
            db.race.findBySeq(results,  function(err, results, fields) {
                if (err) //db 에러
                    res.json(error.getResult(req, 'DB에러가 발생했습니다.', null));
                else if (!results || 1 > results.length || 1 > results.count) //중복 없음
                    res.json(error.getResult(req, '수정에 실패했습니다.', null));
                else
                    res.json(error.getResult(req, null, results[0]));
            });
        }
    );
}

function getFCMMessage(site_name, state) {
    if (state == 0) {
        return {
            title : "배차 취소",
            body : site_name + "에 배차가 취소되었습니다."
        };

    } else if (state == 2) {
        return {
            title : "배차 알림",
            body : site_name + "에 배차되었습니다."
        };

    } else if (state == 3) {
        return {
            title : "운행 시작",
            body : site_name + "에 운행이 시작되었습니다."
        };

    } else if (state == 4) {
        return {
            title : "운행 종료",
            body : site_name + "에 운행이 종료되었습니다."
        };

    }
}

/**
 * GET 배차정보 조회
 * @param {*} req
 * @param {*} res
 */
function getRaceByCompany(req, res){
    console.log(req.query);
    var data = req.query;
    db.race.findByCompany(data.company_idx, function(err, results, fields) {
        if (err) //db 에러
            res.status(500).json(error.getResult(req, 'DB에러가 발생했습니다.', null));
        else if (!results || 1 > results.length || 1 > results.count) //가입되어있지 않으면 에러
            res.status(400).json(error.getResult(req, '배차정보가 없습니다.', null));
        else //가입되어 있으면 로그인 처리
            res.status(200).json(error.getResult(req, null, results));
    });
}
/**
 * GET 사이트 상세 배차정보 조회
 * @param {*} req
 * @param {*} res
 */
function getRaceBySite(req, res){
    console.log(req.query);
    var data = req.query;
    var siteid = req.params.siteid;
    db.race.findBySiteToDriver(siteid, function(err, results, fields) {
        if (err) //db 에러
            res.status(500).json(error.getResult(req, 'DB에러가 발생했습니다.', null));
        else if (!results || 1 > results.length || 1 > results.count) //가입되어있지 않으면 에러
            res.status(400).json(error.getResult(req, '배차정보가 없습니다.', null));
        else //가입되어 있으면 로그인 처리
            res.status(200).json(error.getResult(req, null, results));
    });
}