/**
 * Created by levine on 15. 2. 6..
 */
var _ = require('lodash');
var config = require('../../../config').get();
var error = require('../error');
var async = require('async');
var db = require('../../../dblibs/');
var moment = require('moment');
var util = require('util');
var validate = require('../../../validlibs/');
var encryptLib = require('../../../lib/encrypt');
var random = require('randomstring');
var fcm = require('../../../lib/fcm');

module.exports = [
    {
        method : 'post',
        url : '/api/record/reg',
        func : registRaceRecord,
        middleware : [
        ]
    },
    {
        method : 'get',
        url : '/api/record/:driver_id',
        func : getRaceRecordByDriver,
        middleware : [
        ]
    }
];

/**
 * 운행기록 등록
 * @param req
 * @param res
 */
function registRaceRecord(req, res){
    console.log(req.body);
    var data = req.body;

    ////validate[S]
    //var valresult = validate.race.reg(data);
    //console.log(valresult);
    //if(!valresult.valid)
    //    return res.json(400, error.getResult(req, valresult.msg, null));

    //validate[E]
    var params = [
        data.createAt,
        data.raceIdx,
        data.siteIdx,
        data.driverIdx,
        data.fromVisit,
        data.toVisit
    ];

    db.raceRecord.reg(params,  function(err, results, fields) {
        if (err) //db 에러
            res.json(error.getResult(req, 'DB에러가 발생했습니다.', null));
        else if (!results || 1 > results.length || 1 > results.count) //중복 없음
            res.json(error.getResult(req, '운행 등록에 실패했습니다.', null));
        else
            res.json(error.getResult(req, null, null));
    });
}

/**
 * GET 운행기록 조회
 * @param {*} req
 * @param {*} res
 */
function getRaceRecordByDriver(req, res){
    console.log(req.query);
    var start = req.query.start;
    var driver_id = req.params.driver_id;

    db.raceRecord.findByDriver([driver_id, start], function(err, results, fields) {
        if (err) //db 에러
            res.status(500).json(error.getResult(req, 'DB에러가 발생했습니다.', null));
        else if (!results || 1 > results.length || 1 > results.count) //가입되어있지 않으면 에러
            res.status(400).json(error.getResult(req, '운행기록ㅇㅣ 없습니다.', null));
        else //가입되어 있으면 로그인 처리
            res.status(200).json(error.getResult(req, null, results));
    });
}