/**
 * Created by levine on 15. 2. 6..
 */
var _ = require('lodash');
var config = require('../../../config').get();
var error = require('../error');
var async = require('async');
var db = require('../../../dblibs/');
var moment = require('moment');
var util = require('util');
var validate = require('../../../validlibs/');
var encryptLib = require('../../../lib/encrypt');
var random = require('randomstring');

module.exports = [
    {
        method : 'post',
        url : '/api/truck_user/reg',
        func : registTruckUser,
        middleware : [
        ]
    },
    {
        method : 'post',
        url : '/api/truck_user/login',
        func : loginTruckUser,
        middleware : [
        ]
    }
];

/**
 * 회원가입
 * @param req
 * @param res
 */
function registTruckUser(req, res){
    var data = req.body;

    //validate[S]
    var valresult = validate.truckUser.reg(data);
    console.log(valresult);
    if(!valresult.valid)
        return res.status(400).json(error.getResult(req, valresult.msg, null));

    //validate[E]
    var chkParams = [data.name.toString().trim(), data.contact.toString().trim()];

    async.waterfall([
        //이미 가입된 회원인지 체크
        function(next){
            db.truckUser.loginByInfo(chkParams, function(err, results, fields) {
                if (err) //db 에러
                    res.status(500).json(error.getResult(req, 'DB에러가 발생했습니다.', null));
                else if (!results || 1 > results.length || 1 > results.count) //중복 없음
                    next(null, null);
                else //이미 가입되어 있으면 로그인 처리
                    saveSession(req, res, results[0]);
            });
        },
        //회원가입
        function(result, next){
            if(null != result && null != next)
                next();
            else {
                var queryParams = [
                    data.name,
                    data.contact,
                    data.organizer,
                    data.organizer_idx,
                    data.org_type,
                    data.truck_number,
                    data.truck_type,
                    data.push_token,
                    data.platform,
                    data.device,
                    null
                ];


                db.truckUser.reg(queryParams, function(err, results, fields) {
                    if (err) //db 에러
                        res.status(500).json(error.getResult(req, 'DB에러가 발생했습니다.', null));
                    else if (!results || 1 > results.length || 1 > results.count) //중복 없음
                        res.status(500).json(error.getResult(req, '회원가입에 실패했습니다.', null));
                    else //이미 가입되어 있으면 로그인 처리
                        next(null, results.insertId);
                });
            }
        }],
        function(err, results){
            console.log("regist complete");
            db.truckUser.loginByseq(results,  function(err, results, fields) {
                if (err) //db 에러
                    res.status(500).json(error.getResult(req, 'DB에러가 발생했습니다.', null));
                else if (!results || 1 > results.length || 1 > results.count) //중복 없음
                    res.status(500).json(error.getResult(req, '회원가입에 실패했습니다.', null));
                else //이미 가입되어 있으면 로그인 처리
                    res.status(200).json(error.getResult(req, null, results[0]));
            });
        }
    );
}

/**
 * 로그인
 * @param req
 * @param res
 */
function loginTruckUser(req, res){
    console.log(req.body);
    var data = req.body;

    //validate[S]
    var valresult = validate.truckUser.login(data);
    console.log(valresult);
    if(!valresult.valid)
        return res.json(400, error.getResult(req, valresult.msg, null));

    //validate[E]
    var chkParams = [data.name.toString().trim(), data.contact.toString().trim()];

    async.waterfall([
            //이미 가입된 회원인지 체크
            function(next){
                db.truckUser.loginByInfo(chkParams, function(err, results, fields) {
                    if (err) //db 에러
                        res.status(500).json(error.getResult(req, 'DB에러가 발생했습니다.', null));
                    else if (!results || 1 > results.length || 1 > results.count) //가입되어있지 않으면 에러
                        res.status(500).json(error.getResult(req, '회원가입을 먼저 해주세요.', null));
                    else //가입되어 있으면 로그인 처리
                        next(null, results[0]);
                });
            },
            function(result, next){
                db.truckUser.modInfoToPushTokenBySeq([data.push_token, result.id], function(err, results, fields) {
                    if (err) //db 에러
                        res.status(500).json(error.getResult(req, 'DB에러가 발생했습니다.', null));
                    else if (!results || 1 > results.length || 1 > results.count) //가입되어있지 않으면 에러
                        res.status(500).json(error.getResult(req, '회원가입을 먼저 해주세요.', null));
                    else //가입되어 있으면 로그인 처리
                        next(null, result);
                });
            }
        ],
        function(err, results){
            console.log("login complete")
            res.status(200).json(error.getResult(req, null, results));
        }
    );
}

function saveSession(req, res, param){
    console.log("exists user");
    res.status(200).json(error.getResult(req, null, param));
}
