/**
 * Created by levine on 15. 2. 6..
 */
var _ = require('lodash');
var config = require('../../../config').get();
var error = require('../error');
var async = require('async');
var db = require('../../../dblibs/');
var moment = require('moment');
var util = require('util');
var validate = require('../../../validlibs/');
var encryptLib = require('../../../lib/encrypt');
var random = require('randomstring');
var fcm = require('../../../lib/fcm');

module.exports = [
    {
        method : 'post',
        url : '/api/consite/reg',
        func : registConSite,
        middleware : [
        ]
    },
    {
        method : 'post',
        url : '/api/consite/find',
        func : findConSiteByCompany,
        middleware : [
        ]
    },
    {
        method : 'post',
        url : '/api/consite/detail',
        func : findConSiteById,
        middleware : [
        ]
    },
    {
        method : 'post',
        url : '/api/consite/trucktype',
        func : getTruckType,
        middleware : [
        ]
    },
    {
        method : 'post',
        url : '/api/consite/sitetype',
        func : getSiteType,
        middleware : [
        ]
    },
    {
        method : 'post',
        url : '/api/consite/soiltype',
        func : getSoilType,
        middleware : [
        ]
    },
    {
        method : 'get',
        url : '/api/consite/site/:site_idx',
        func : getConstructSiteDetail,
        middleware : [
        ]
    },
    {
        method : 'get',
        url : '/api/consite/all',
        func : getConstructSites,
        middleware : [
        ]
    },
    {
        method : 'post',
        url : '/api/consite/delete',
        func : deleteConsite,
        middleware : [
        ]
    }
];

/**
 * 회원가입
 * @param req
 * @param res
 */
function registConSite(req, res){
    
    console.log(req.body);
    var orgUser = JSON.parse(req.header('user'));
    
    console.log(orgUser);
    var data = req.body;
    
    //validate[S]
    var valOrgUser = validate.orgUser.login(orgUser);
    console.log(valOrgUser);
    if(!valOrgUser.valid)
        return res.json(400, error.getResult(req, valOrgUser.msg, null));


    var valParams = validate.conSite.reg(data);
    console.log(valParams);
    if(!valParams.valid)
        return res.json(400, error.getResult(req, valParams.msg, null));

    //validate[E]
    var chkParams = [unescape(orgUser.name).toString().trim(), orgUser.password.toString().trim()];

    async.waterfall([
        //회원체크
        function(next){
            db.orgUser.loginByInfo(chkParams, function(err, results, fields) {
                if (err) //db 에러
                    res.status(500).json(error.getResult(req, 'DB에러가 발생했습니다.', null));
                else if (!results || 1 > results.length || 1 > results.count) //중복 없음
                    res.status(401).json(error.getResult(req, '로그인 후 등록해 주세요.', null));
                else //이미 가입되어 있으면 로그인 처리
                    next(null, results[0]);
            });
        },
        //등록
        function(result, next){
            console.log(result);
            var queryParams = [
                result.id,
                data.lat,
                data.lon,
                data.addr,
                data.name,
                data.contact,
                data.to_lat,
                data.to_lon,
                data.to_addr,
                data.to_name,
                data.site_type,
                data.soil_type,
                data.truck_type,
                data.fee,
                data.quantity,
                1 ,//상태값은 최초 생성시엔 무조건 1
                data.need_quantity
            ];


            db.conSite.reg(queryParams, function(err, results, fields) {
                if (err) //db 에러
                    res.status(500).json(error.getResult(req, 'DB에러가 발생했습니다.', null));
                else if (!results || 1 > results.length || 1 > results.count)
                    res.status(500).json(error.getResult(req, '현장등록에 실패했습니다.', null));
                else //이미 가입되어 있으면 로그인 처리
                    next(null, results.insertId);
            });
        }],
        function(err, results){
            console.log("regist complete");
            db.conSite.sitesBySeq(results,  function(err, results, fields) {
                if (err) //db 에러
                    res.status(500).json(error.getResult(req, 'DB에러가 발생했습니다.', null));
                else if (!results || 1 > results.length || 1 > results.count)
                    res.status(500).json(error.getResult(req, '현장등록에 실패했습니다.', null));
                else {//등록 완료
                    var fcmBody = {
                        idx : results[0].id,
                        filted : false,
                        displayName : results[0].name,
                        displayToName : results[0].to_name,
                        fee : results[0].fee,
                        createAt : results[0].createAt
                    };
                    var notification = {
                        title : "새로운 현장 알림",
                        body : results[0].name + "에 트럭이 필요합니다."
                    };

                    var noti = [results[0].id, 0, 1, notification.body];
                    db.conSiteNoti.reg(noti, function(err, results, fields){
                        if (err)
                            console.log('err : ' + err);
                        else
                            console.log('save complete')
                    });

                    fcm.sendTopic("contruct_site", fcmBody, notification, function(err, response){
                        if (err) {
                            console.log(err);
                            console.log("Something has gone wrong!");
                        } else {
                            console.log(response);
                            console.log("Successfully sent with response: ", response);
                        }
                    });
                    res.status(200).json(error.getResult(req, null, results[0]));
                }
            });
        }
    );
}

/**
 * 회사정보로 현장정보 조회
 * @param req
 * @param res
 */
function findConSiteByCompany(req, res){
    var orgUser = JSON.parse(req.header('user'));
   
    console.log(orgUser);
    
    //validate[S]
    var valresult = validate.orgUser.login(orgUser);
    console.log(valresult);
    if(!valresult.valid)
        return res.json(400, error.getResult(req, valresult.msg, null));

    //validate[E]
    var chkParams = [unescape(orgUser.name).toString().trim(), orgUser.password.toString().trim()];

    async.waterfall([
            function(next){
                db.orgUser.loginByInfo(chkParams, function(err, results, fields) {
                    //console.log('loginByInfo',results);
                    if (err) //db 에러
                        res.json(500, error.getResult(req, 'DB에러가 발생했습니다.', null));
                    else if (!results || 1 > results.length || 1 > results.count) //가입되어있지 않으면 에러
                        res.json(500, error.getResult(req, '회원가입을 먼저 해주세요.', null));
                    else //가입되어 있으면 로그인 처리
                        next(null, results[0]);
                });
            }],
        function(err, results){
            console.log("find complete")
            db.conSite.sitesByComp(results.id,  function(err, results, fields) {
                //console.log('sitesByComp',results);
                if (err) //db 에러
                    res.status(500).json(error.getResult(req, 'DB에러가 발생했습니다.', null));
                else if (!results || 1 > results.length || 1 > results.count)
                    res.status(500).json(error.getResult(req, '현장이 존재하지 않습니다.', null));
                else //이미 가입되어 있으면 로그인 처리
                    res.status(200).json(error.getResult(req, null, results));
            });
        }
    );
}
/**
 * 회사정보로 현장정보 조회
 * @param req
 * @param res
 */
function findConSiteById(req, res){
    var orgUser = JSON.parse(req.header('user'));
   
    console.log(orgUser);
    
    //validate[S]
    var valresult = validate.orgUser.login(orgUser);
    console.log(valresult);
    if(!valresult.valid)
        return res.json(400, error.getResult(req, valresult.msg, null));

    //validate[E]
    var chkParams = [unescape(orgUser.name).toString().trim(), orgUser.password.toString().trim()];

    async.waterfall([
            function(next){
                db.orgUser.loginByInfo(chkParams, function(err, results, fields) {
                    if (err) //db 에러
                        res.json(500, error.getResult(req, 'DB에러가 발생했습니다.', null));
                    else if (!results || 1 > results.length || 1 > results.count) //가입되어있지 않으면 에러
                        res.json(500, error.getResult(req, '회원가입을 먼저 해주세요.', null));
                    else //가입되어 있으면 로그인 처리
                        next(null, results[0]);
                });
            }],
        function(err, results){
            console.log("find complete")
            console.log(req.body.id);
            db.conSite.sitesBySeq(req.body.id,  function(err, results, fields) {
                if (err) //db 에러
                    res.status(500).json(error.getResult(req, 'DB에러가 발생했습니다.', null));
                else if (!results || 1 > results.length || 1 > results.count)
                    res.status(500).json(error.getResult(req, '현장검색에 실패했습니다.', null));
                else //이미 가입되어 있으면 로그인 처리
                    res.status(200).json(error.getResult(req, null, results));
            });
        }
    );
}
/**
 * 덤프종류 조회
 * 우선 전부조회
 * @param {*} req 
 * @param {*} res 
 */
function getTruckType(req, res){
    console.log(req.body);
   
    var orgUser = JSON.parse(req.header('user'));
    
    console.log(orgUser);

    var valresult = validate.orgUser.login(orgUser);
    
    console.log(valresult);
    if(!valresult.valid)
        return res.json(400, error.getResult(req, valresult.msg, null));

    async.waterfall([
        //조회
        function(next){
            db.conSite.getTruckType({}, function(err, results, fields) {
                if (err) //db 에러
                    res.status(500).json(error.getResult(req, 'DB에러가 발생했습니다.', null));
                else if (!results || 0 == results.length ) 
                    res.status(401).json(error.getResult(req, '데이터가 존재하지 않습니다.', null));
                else //이미 가입되어 있으면 로그인 처리
                    res.status(200).json(error.getResult(req, null, results));
            });
        }    
    ]);
}
/**
 * 현장 ㅌㅏㅇㅣㅂ 조회
 * @param {*} req 
 * @param {*} res 
 */
function getSiteType(req, res){
    console.log(req.body);
    var orgUser = JSON.parse(req.header('user'));
     
    console.log(orgUser);
    var valresult = validate.orgUser.login(orgUser);
    console.log(valresult);
    if(!valresult.valid)
        return res.json(400, error.getResult(req, valresult.msg, null));

    async.waterfall([
        //조회
        function(next){
            db.conSite.getSiteType({}, function(err, results, fields) {
                if (err) //db 에러
                    res.status(500).json(error.getResult(req, 'DB에러가 발생했습니다.', null));
                else if (!results || 0 == results.length ) 
                    res.status(401).json(error.getResult(req, '데이터가 존재하지 않습니다.', null));
                else //이미 가입되어 있으면 로그인 처리
                    res.status(200).json(error.getResult(req, null, results));
            });
        }    
    ]);
}
/**
 * 토석 조회
 * @param {*} req 
 * @param {*} res 
 */
function getSoilType(req, res){
    console.log(req.body);
    var orgUser = JSON.parse(req.header('user'));
    
    console.log(orgUser);
    var valresult = validate.orgUser.login(orgUser);
    console.log(valresult);
    if(!valresult.valid)
        return res.json(400, error.getResult(req, valresult.msg, null));

    async.waterfall([
        //조회
        function(next){
            db.conSite.getSoilType({}, function(err, results, fields) {
                if (err) //db 에러
                    res.status(500).json(error.getResult(req, 'DB에러가 발생했습니다.', null));
                else if (!results || 0 == results.length ) 
                    res.status(401).json(error.getResult(req, '데이터가 존재하지 않습니다.', null));
                else //이미 가입되어 있으면 로그인 처리
                    res.status(200).json(error.getResult(req, null, results));
            });
        }    
    ]);
}
/**
 * GET ㅎㅕㄴㅈㅏㅇ ㅈㅗㅎㅗㅣ
 * @param {*} req
 * @param {*} res
 */
function getConstructSiteDetail(req, res){
    var query = req.query;
    var path = req.params;

    var valresult = validate.conSite.find(path);
    console.log(valresult);
    if(!valresult.valid)
        return res.json(400, error.getResult(req, valresult.msg, null));

    async.parallel({
        conSite : function(callback){
            db.conSite.sitesBySeq(path.site_idx, function(err, results, fields) {
                callback(err, results[0]);
            });
        },
        raceCall : function(callback){
            db.race.findByDriverNSite([path.site_idx, query.driver_idx], function(err, results, fields) {
                callback(err, results[0]);
            });
        }
    }, function(errs, results){
        if (errs) //db 에러
            res.status(500).json(error.getResult(req, 'DB에러가 발생했습니다.', null));
        else if (!results || null == results.conSite)
            res.status(401).json(error.getResult(req, '데이터가 존재하지 않습니다.', null));
        else //이미 가입되어 있으면 로그인 처리
            res.status(200).json(error.getResult(req, null, results));

    });
}
/**
 * GET ㅎㅕㄴㅈㅏㅇ ㅈㅗㅎㅗㅣ
 * @param {*} req
 * @param {*} res
 */
function getConstructSites(req, res){
    console.log(req.query);
    var query = req.query;

    db.conSite.allSites(function(err, results, fields) {
        if (err) //db 에러
            res.status(500).json(error.getResult(req, 'DB에러가 발생했습니다.', null));
        else if (!results || 0 == results.length )
            res.status(401).json(error.getResult(req, '데이터가 존재하지 않습니다.', null));
        else //이미 가입되어 있으면 로그인 처리
            res.status(200).json(error.getResult(req, null, results));
    });
}
/**
 * 현장 삭제
 * @param {data.id} req 
 * @param {*} res 
 */
function  deleteConsite(req, res){
    var orgUser = JSON.parse(req.header('user'));
    console.log(req.body);
    console.log(orgUser);
    var data = req.body;
    //validate[S]
    var valresult = validate.orgUser.login(orgUser);
    console.log(valresult);
    if(!valresult.valid)
        return res.json(400, error.getResult(req, valresult.msg, null));

    //validate[E]
    var chkParams = [unescape(orgUser.name).toString().trim(), orgUser.password.toString().trim()];

    async.waterfall([
        function(next){
                db.orgUser.loginByInfo(chkParams, function(err, results, fields) {
                    //console.log('loginByInfo',results);
                    if (err) //db 에러
                        res.json(500, error.getResult(req, 'DB에러가 발생했습니다.', null));
                    else if (!results || 1 > results.length || 1 > results.count) //가입되어있지 않으면 에러
                        res.json(500, error.getResult(req, '회원가입을 먼저 해주세요.', null));
                    else //가입되어 있으면 로그인 처리
                        next(null, results[0]);
                });
            }],
        function(err, results){
            var queryParams = [Number(data.id)];
            db.conSite.deleteSite(queryParams,  function(err, results, fields) {
                console.log('deleteSite',results);
                if (err) //db 에러
                    res.status(500).json(error.getResult(req, 'DB에러가 발생했습니다.', null));
                else if (!results || 1 > results.length || 1 > results.count)
                    res.status(500).json(error.getResult(req, '현장삭제에 실패했습니다.', null));
                else //이미 가입되어 있으면 로그인 처리
                    res.status(200).json(error.getResult(req, null, results));
            });
        }
    );
}