'use strict';
var express = require('express');
var session = require('express-session');
//var session = require('cookie-session');
var cookieParser = require('cookie-parser');

var logger = require('../lib/logger');
var routes = require('./routes/');
var authenticator = require('./middleware/authentication');
var path = require('path');

var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var morgan = require('morgan');
var loggerFormat = '[:method][:url][:user-agent][:status] : :response-time ms';
var redis = require('redis');
var redisStore = require('connect-redis')(session);

var multer  = require('multer');
var error = require('./routes/error');

var client = redis.createClient();
var cors = require('cors')
var app = express();


app.use(cors())
//express default config
app.set('views', path.join(__dirname, '/public'));
app.set('view engine', 'ejs');
app.set('view cache');
app.use(express.static(__dirname + '/public'));
app.use(morgan({stream : logger.stream, format : loggerFormat}));
app.use(bodyParser.json({
        type : [
            'application/vnd.api+json',
            'application/json'
        ]
    }
));
app.use(bodyParser.urlencoded({
    extended: true,
    type : 'application/x-www-form-urlencoded'
}));
app.use(bodyParser.text());
app.use(bodyParser.raw());

//session init
app.set('trust proxy', 1) // trust first proxy
app.use(session({
    secret: 'truckcall',//app.cookie,
    resave: false,
    saveUninitialized: false,
    store: new redisStore({ host: 'localhost', port: 6379, client: client })
}));

// init시 초기화됨
app.use(methodOverride());
//express default config end

//custom middleware
//app.use(authenticator());
//app.use(adminAuth());

//express routes
routeCustom(routes.apiTruckUser);
routeCustom(routes.apiOrgUser);
routeCustom(routes.apiConSite);
routeCustom(routes.apiConSiteNoti);
routeCustom(routes.apiRaceCall);
routeCustom(routes.apiRaceRecord);
routeCustom(routes.apiFcmTest);
module.exports.app = app;


function routeCustom(list) {
    for(var i= 0,len=list.length;i<len;++i) {
        var item = list[i];
        app[item.method](item.url, item.middleware, item.func);
    }
}