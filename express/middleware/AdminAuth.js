//인증 시스템임

var async = require('async');
var error = require('../routes/error');
var _ = require('lodash');
var dbcon = require('../../dblibs/');
var moment = require('moment');
var util = require('util');
var validate = require('../../validlibs/');
var encryptLib = require('../../lib/encrypt');
var random = require('randomstring');

module.exports = function(/*options*/) {
    /*
     if (options && typeof options !== 'object') {
     options = { format: options };
     } else {
     options = options || {};
     }
     */
    return authenticator;
};
var authenticator = function( req, res, next) {
    var url = req.url;
    //return next();
    //url pass
    if(url.indexOf('.js') > -1 || url.indexOf('.css') > -1 || url.indexOf('.jpg') > -1 || url.indexOf('.gif') > -1 || url.indexOf('.png') > -1 || url.indexOf('.ico') > -1
        || url.indexOf('/api/cms/reg') > -1 || url.indexOf('/api/cms/login') > -1) { //} || url.indexOf('/api/cms/sms') > -1 ){
        next();
    } else {

        if(url == '/cms' || url == '/cms/login' || url == '/api/cms/login' || 0 > url.indexOf('cms')){
            next();
        } else {
            console.info('auth url', url);

            //세션이 없을때.
            if(_.isUndefined(req.session) || _.isNull(req.session)){
                console.log('req.session empty');
                return res.redirect('/cms/login');

            } else {
                //세션은 있으나 로그인 정보가 없을 때.
                if(_.isUndefined(req.session.adtoken) || _.isNull(req.session.adtoken) ) {
                    console.log('empty admin token');
                    req.session.originUrl = req.url;
                    return res.redirect('/cms/login');
                }

                //세션의 정보를 확인하고, 유효시간이 넘지 않았다면 유효시간을 갱신,
                var params = [req.session.adtoken];
                async.waterfall([
                    function(cb){
                        dbcon.admin.loginByToken(params, function(err, result){
                            if(err || !result || 1 > result.count) {
                                req.session.originUrl = req.url;
                                res.redirect('/cms/login');
                            } else
                                cb(null, result);
                        });
                    },
                    function(result, cb){
                        console.log(result.result[0].access_token);
                        var curTime = new Date().getTime();

                        //세션에 설정한 만료시간이 현재시간을 넘어갔다면, 로그인으로 넘긴다.
                        if((_.isUndefined(req.session.ad_expire) || _.isNull(req.session.ad_expire) || curTime > req.session.ad_expire)) {

                            console.log('admin session expired');
                            req.session.originUrl = req.url;
                            res.redirect('/cms/login');
                        } else {
                            req.session.adtoken = result.result[0].access_token;
                            req.session.ad_expire = new Date().getTime() + 3600000*24;
                            req.session.ad_actime = new Date().getTime();
                            cb(null, null);
                        }
                    }
                ], function(err, result){
                    console.info('req.session', JSON.stringify(req.session));
                    next();
                });
            }
        }
    }

};