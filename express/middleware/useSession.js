/**
 * Created by levine on 15. 2. 9..
 */
'use strict';
var app = express();

app.use(session({
    secret: 'truckcall-service',
    resave: false,
    saveUninitialized: true,
    cookie: { secure:true, maxAge: 360000 }
}));

exports.main = function(req, res, next) {
    console.log(req.session);
    next();
};

