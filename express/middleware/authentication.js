//인증 시스템임

var async = require('async');
var error = require('../routes/error');
var _ = require('lodash');
var dbcon = require('../../dblibs/');
var moment = require('moment');
var util = require('util');
var validate = require('../../validlibs/');
var random = require('randomstring');

module.exports = function() {
    return authenticator;
};
var authenticator = function( req, res, next) {
    var url = req.url;

    //url pass
    if(url == '/' || url.indexOf('/index') > -1 || url == '/terms' || url == '/policy' || url == '/faq' || url == '/api/order/mobile/noti' || url == '/order/comp' || url == '/ask' || url == '/web/vbnkCfn' || url == '/cauly' ||
        url == '/login' || url == '/signin' || url == '/password' || url == '/api/user/reg' || url == '/api/user/login' || url == '/api/user/findPw' || url == '/web/order/comp' || url == '/subs/comp' || url == '/subs/mcomp' ||
        url.indexOf('/survey/save') > -1 || url.indexOf('/subs') > -1 || (url.indexOf('/order') > -1 && 0 > url.indexOf('/api/order')) || url.indexOf('/event') > -1 || url.indexOf('/test') > -1 ||
        url.indexOf('.js') > -1 || url.indexOf('.css') > -1 || url.indexOf('.jpg') > -1 || url.indexOf('.gif') > -1 || url.indexOf('.png') > -1 || url.indexOf('.ico') > -1){

        console.info('req.session', JSON.stringify(req.session));
        if(!_.isUndefined(req.session) && !_.isNull(req.session))
            req.session.originUrl = null;

        next();
    } else if (0 > url.indexOf('cms')) {
        console.info('auth url', url);
        console.info('protocol', req.protocol);

        //세션이 없을때.
        if(_.isUndefined(req.session) || _.isNull(req.session)){
            console.log('req.session empty ===========> ');
            return res.redirect('/login');

        } else {

            console.info('session', JSON.stringify(req.session));

            //세션은 있으나 로그인 정보가 없을 때.
            if(_.isUndefined(req.session.actoken) || _.isNull(req.session.actoken) ) {
                console.log('empty token ===========> ');
                req.session.originUrl = req.url;
                return res.redirect('/login');
            }

            //세션의 정보를 확인하고, 유효시간이 넘지 않았다면 유효시간을 갱신,
            var params = [req.session.actoken];
            async.waterfall([
                function(cb){
                    dbcon.user.loginByToken(params, function(err, result){
                        if(err || !result || 1 > result.count) {
                            console.log('empty user ======> ');
                            req.session.originUrl = req.url;
                            res.redirect('/login');
                        } else
                            cb(null, result);
                    });
                },
                function(result, cb){
                    console.log(result.result[0].access_token);
                    var curTime = new Date().getTime();

                    //세션에 설정한 만료시간이 현재시간을 넘어갔다면, 로그인으로 넘긴다.
                    if((_.isUndefined(req.session.expire) || _.isNull(req.session.expire) || curTime > req.session.expire) &&
                        _.isUndefined(req.session.loginSave) && _.isNull(req.session.loginSave)) {

                        console.log('session expired ========> ');
                        req.session.originUrl = req.url;
                        res.redirect('/login');
                    } else {
                        req.session.actoken = result.result[0].access_token;
                        req.session.expire = new Date().getTime() + 3600000*24;
                        req.session.actime = new Date().getTime();
                        cb(null, null);
                    }
                }
            ], function(err, result){
                console.info('req.session', JSON.stringify(req.session));
                if(!_.isUndefined(req.session) && !_.isNull(req.session))
                    req.session.originUrl = null;

                next();
            });
        }
    } else {
        if(!_.isUndefined(req.session) && !_.isNull(req.session))
            req.session.originUrl = null;

        next();
    }

};