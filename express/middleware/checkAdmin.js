'use strict';

exports.main = function(req, res, next) {
    if(!req.session.hasOwnProperty('admin')) {
        if(req.hasOwnProperty('output')) {
            req.output.session = false;
            req.error(res, "Error: No session");
        } else {
            res.redirect('/web/system/login');
        }
        return;
    }

    req.session.save(function() {
        if(req.hasOwnProperty('output')) {
            req.output.session = true;
        }
        next();
    });
};