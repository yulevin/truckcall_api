'use strict';
var app = require('./express').app;
var logger = require('./lib/logger');
var cluster = require('cluster');
var https = require('https');
var http = require('http');
var socketIO = require('socket.io')

if(cluster.isMaster) {
    var cpus = require('os').cpus().length;
    for (var i = 0; i < cpus; i++) {
        cluster.fork();
    }

    Object.keys(cluster.workers).forEach(function(id) {
        cluster.workers[id].on('message', function(msg){
            this.send(msg);
        });
    });
} else {
    var server = app.listen(process.env.PORT, function() {
        logger.info('truckcall api http was running in port : %d', process.env.PORT);
        logger.info('truckcall api http was running mode in : %s', process.env.NODE_ENV);
    });
    // var io = require('socket.io')(server);
    // app.set('socketio', io);
   // app.use((req, res, next)=>{ res.locals['socketio'] = io; next(); });
   const io = socketIO(server);
    /**
     * GPS socket io Server
     * 1. connect:gps : param(json) race_call : raceCall번호
     * 2. send:gps : 메시지 정의 부탁
     * 3. disconnect:gps : 연결종료 
     */
   //socket ~
    io.on('connection', socket => {
        console.log('call connect');
        //1. 커넥트{방연결)
        //{race_call : racecall.id}
        socket.on('connect:gps', function(data) {
            console.log('connect:gps join', 'gps-room.no:'+data.race_call);
            socket.join('gps-room.no:'+data.race_call);
        });

        // current gps message
        socket.on('send:gps', function(data) {
            console.log('send:gps', data);
            io.sockets.in('gps-room.no:'+data.race_call).emit('send:gps', data);
        });

        //connect 종료
        socket.on('disconnect:gps', function(data) {
            console.log('disconnect', data);
            socket.disconnect();
        });
    })
}