var FCM = require('fcm-push');

var serverKey = 'AAAAVd9RngA:APA91bEVeTYqSOSGPlulJEY8PUVKDo7o_YX9MQfnC92U0DrHDRcPvQZE1oxeCQyiUGGlo-8_YyY_n-ylSdG8MHsvr42nRDMiGLsboDoWjOHO6p46vI0mHCgeXgws5hDCja_icGlmMn3LDMxSEt55voFMCEbtz3En8w';
var fcm = new FCM(serverKey);


//callback style

exports.test = function(msg, cb) {
    console.log(msg);
    fcm.send(msg, cb);
};

exports.sendSingle = function(receiver, bodyData, notiData, cb){

    var message = {
        to: receiver,
        collapse_key: 'single',
        data: bodyData,
        notification: notiData,
        priority: 'high'
    };

    console.log(message);
    fcm.send(message, cb);
};

exports.sendBulk = function(receivers, bodyData, notiData, cb){

    var message = {
        registration_ids: receivers,
        collapse_key: 'bulk',
        data: bodyData,
        notification: notiData,
        priority: 'high'
    };

    console.log(message);
    fcm.send(message, cb);
};

exports.sendTopic = function(topic, bodyData, notiData, cb){

    var message = {
        to: '/topics/' + topic,
        collapse_key: topic,
        data: bodyData,
        notification: notiData,
        priority: 'high'
    };

    console.log(message);
    fcm.send(message, cb);
};