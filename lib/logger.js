'use strict';

var winston = require('winston');
var config = require('../config').get();
var path = require('path');

var logger = new (winston.Logger)({
});

logger._init = function() {
    var self = this;
    logger.stream = {
        write : function(message) {
            self.info(message.slice(0, -1));
        }
    };
    logger.add(winston.transports.Console, {
        'timestamp' : function() { return new Date(); }
    });
    if(config.fileLog) {
        var logPath = path.resolve('./' + (config.logPath?config.logPath:'logs'));
        var logFileName = config.logFile?config.logFile:'log.out';
        logger.add(winston.transports.File, {
            filename : logPath + '/' + logFileName
            //handleExceptions : true
        });
    }

};
logger._init();
//logger.stream = winstonStream;
logger.info('Logging setting complete.');

module.exports = logger;