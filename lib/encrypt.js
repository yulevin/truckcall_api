/**
 * Created by levine on 15. 2. 6..
 */

var node_cryptojs = require('node-cryptojs-aes');
var crypto = require('crypto');
var config  = require('../config').get('production');

exports.encrypt = encrypt;
exports.decrypt = decrypt;
exports.genBaseKey = generateBaseKey;


function generateBaseKey(){
    // generate random passphrase binary data
    var r_pass = crypto.randomBytes(256);

    // convert passphrase to base64 format
    var r_pass_base64 = r_pass.toString("base64");

    // node-cryptojs-aes main object;
    var CryptoJS = node_cryptojs.CryptoJS;

    // custom json serialization format
    var JsonFormatter = node_cryptojs.JsonFormatter;

    // encrypt plain text with passphrase and custom json serialization format, return CipherParams object
    // r_pass_base64 is the passphrase generated from first stage
    // message is the original plain text
    var encrypted = CryptoJS.AES.encrypt(config.basicSalt, r_pass_base64, { format: JsonFormatter });

    // convert CipherParams object to json string for transmission
    var encrypted_json_str = encrypted.toString();

    return encrypted_json_str;

}


/**
 * 암호화하기.
 * @param param
 * @returns {string|String|*}
 */
function encrypt(param){
    //generate base key
    var salt = generateBaseKey();

    // node-cryptojs-aes main object;
    var CryptoJS = node_cryptojs.CryptoJS;

    // custom json serialization format
    var JsonFormatter = node_cryptojs.JsonFormatter;

    // encrypt plain text with passphrase and custom json serialization format, return CipherParams object
    // r_pass_base64 is the passphrase generated from first stage
    // message is the original plain text
    var encrypted = CryptoJS.AES.encrypt(param, salt, { format: JsonFormatter });

    // convert CipherParams object to json string for transmission
    var encrypted_json_str = encrypted.toString();

    var result = {
        result : encrypted_json_str,
        baseKey : salt
    };

    return result;
}

/**
 * 복호화하기.
 * @param eParam
 * @returns {string}
 */
function decrypt(eParam, seed){
    // node-cryptojs-aes main object;
    var CryptoJS = node_cryptojs.CryptoJS;

    // custom json serialization format
    var JsonFormatter = node_cryptojs.JsonFormatter;

    // decrypt data with encrypted json string, passphrase string and custom JsonFormatter
    var decrypted = CryptoJS.AES.decrypt(eParam, seed, { format: JsonFormatter });

    // convert to Utf8 format unmasked data
    var decrypted_str = CryptoJS.enc.Utf8.stringify(decrypted);

    return decrypted_str;
}

