var mysql = require('mysql');

var serverOpt = {
    host     : 'wezonedb1.cnbhy4balggr.ap-northeast-2.rds.amazonaws.com',
    user     : 'root',
    password : 'vine0425!',
    database : 'truck_call',
    connectionLimit : 50
};

var connectionPool = mysql.createPool(serverOpt);

testDate();

exports.db = connect;
exports.nonPrepare = nonPrepare;

function connect(query, params, cb){
    connectionPool.getConnection(function(err, connection) {
        if (err) {
            console.error("err : " + new Date());
            console.error(err);
            cb(err, null, null);
            return;
        }

        // Use the connection
        connection.query(query, params, function (error, results, fields) {
            // When done with the connection, release it.
            connection.release();

            // Handle error after the release.
            if (error) {
                console.error("query err : " + new Date());
                console.error(error);
                cb(error, null, null);
            } else
                cb(null, results, fields);

            // Don't use the connection here, it has been returned to the pool.
        });
    });
    //
    //var connection = mysql.createConnection(serverOpt);
    //connection.connect(function(err){
    //    if (err)
    //        console.log(err);
    //});
    //connection.query(query, params, function(err, results, fields){
    //    if (err)
    //        console.log(err);
    //
    //    cb(err, results, fields);
    //    connection.end();
    //});
}

function nonPrepare(query, cb){
    connectionPool.getConnection(function(err, connection) {
        if (err) {
            console.error("err : " + new Date());
            console.error(err);
            cb(err, null, null);
            return;
        }
        // Use the connection
        connection.query(query, function (error, results, fields) {
            // When done with the connection, release it.
            connection.release();

            // Handle error after the release.
            if (error) {
                console.error("query err : " + new Date());
                console.error(error);
                cb(error, null, null);
            } else
                cb(null, results, fields);

            // Don't use the connection here, it has been returned to the pool.
        });
    });
}

function testDate() {
    connectionPool.getConnection(function(err, connection) {
        if (err) {
            console.error("testdate err : " + new Date());
            console.error(err);
            cb(err, null, null);
            return;
        }

        // Use the connection
        connection.query('SELECT NOW() as now', function (error, results, fields) {
            // When done with the connection, release it.
            connection.release();
            if (error) {
                console.error("query err : " + new Date());
                console.error(error);
            } else
                console.log("NOW : " + results[0].now);
        });
    });
    //var connection = mysql.createConnection(serverOpt);
    //connection.query('SELECT NOW() as now', function(err, results, fields){
    //    if(err) throw err;
    //
    //    console.log("NOW : " + results[0].now);
    //    connection.end();
    //});
}