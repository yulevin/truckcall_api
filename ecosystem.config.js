module.exports = {
  apps : [{
    name      : 'truckcall-api',
    script    : 'server.js',
    env: {
      NODE_ENV: 'development',
      PORT : 5051
    },
    env_production : {
      NODE_ENV: 'production',
      PORT : 5051
    },
    max_memory_restart : '300M',
    log_date_format : "YYYY-MM-DD HH:mm"
  }]

  // deploy : {
  //   production : {
  //     user : 'node',
  //     host : '212.83.163.1',
  //     ref  : 'origin/master',
  //     repo : 'git@github.com:repo.git',
  //     path : '/var/www/production',
  //     'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
  //   }
  // }
};
