#삭스핀 프로젝트(양말)
###문서 작성일 : 2015-02-6
====================================

초기 작업.

forked by me3. origin by hush_server.

##TO-DO
mvp스토리에 따른 프로세스 개발

+ 회원가입
+ 로그인
+ 단일 주문
+ 구독 주문
+ 주문 확인인
+ 환불 신청
+ 구독 취소
+ 고객센터(문의)

+ cms 주문확인
+ cms 구독확인
+ cms 환불 처리
+ cms 구독취소 처리
+ cms 온라인 문의 처리 

##Server start
+ 
`
sudo node server.js
`
 or 
`
sudo forever start server.js
`

##Database
+ Main repository : postgresql 9.1
+ Cache : not yet

##서버 구성도
+ config : 각 모드별 설정값 모음. 모드는 모두 `development(개발)` 과 `local(로컬테스트)` , `production(상용)` 가 있다.
+ dist : 배포용 파일(not require)
+ express : express 관련 routes, model, middleware, views, public data 등이 있음.
+ lib : postgresql, logger(winston)등의 설정파일
+ dblibs : db 쿼리를 가진 일종의 dao객체
+ logs : config.fileLog가 true일 때 log들이 저장되는 파일
+ test : 테스트 파일
+ port 정보 : 5051 포트를 활용한다.